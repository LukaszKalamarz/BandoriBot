BandoriBot 0.1

Weekend type project. 
This is an application written to farm in-game resources. It assumes that You are using Nox as an Android emulator.
Main focus of this bot is to farm easy songs by doing full combo (and efficiently farming in-game gold). Currently bot is in early stage of development.

File vision.py and windowcapture.py are modified versions of files taken from https://github.com/learncodebygaming/opencv_tutorials (check his YT channel)

TODO:
- implement CLI for more user friendly selection of modes
- implement efficient farming mode
- implement new types of Lives (once given type will be available)
- implement LiveBoost replenish and selection (for efficient farming)
