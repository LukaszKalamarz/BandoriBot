# main.py
import cv2
import numpy as np
from windowcapture import WindowCapture
from vision import findClickPositions
from PIL import Image
import time
import os
import pywinauto
import pyautogui
import win32gui
import threading

def DetectAndClick(imageFile, flag = None,region = None, timeout = 100):
    tmp = SearchButton(imageFile, flag,region, timeout)
    if tmp == -1:
        return tmp
    else:
        for i in range(len(tmp)):
            if region:
                pywinauto.mouse.click(button='left', coords=(tmp[i][0]+region[2], tmp[i][1]+region[0]))
            else:
                pywinauto.mouse.click(button='left', coords=(tmp[i][0], tmp[i][1]))

def SearchButton(imageFile, flag=None,region=None, timeout = 100):
    i= 0
    if (flag == True):
        return 0
    while(i < timeout):
        i+=1
        screenshot1 = wincap.get_screenshot(region)

        points = findClickPositions(
                imageFile, 
                screenshot1, 
                threshold=0.75)
        
        if (points):
            print('Found ' , imageFile)
            flag = True
            return points
    return -1

def SwitchSong():
    pyautogui.moveTo(400, 400)
    pyautogui.click()
    pyautogui.scroll(2)

def SelectNoFullComboSong():
    print('Choosing song')
    while (True):
        tmp = SearchButton('FullCombo.jpg',timeout = 20)
        if tmp == -1 or len(tmp) >=2:
            print('This is a good song')
            break
        else:
            print('Switching song')
            SwitchSong()
        break

def SelectEasy():
    if (SearchButton('EasyOff.jpg', timeout = 20) == -1):
        print('Easy mode selected')
    else:
        print('Selecting Easy mode')
        DetectAndClick('EasyOff.jpg')
    time.sleep(0.25)

def SelectDifficulty(difficulty):
    if difficulty == 'easy':
        SelectEasy()
    else:
        pass #not implemented yet

def SelectLive(difficulty):
    DetectAndClick('multi.jpg')
    time.sleep(0.25)
    if SearchButton('spec.jpg', timeout = 20) != -1:
        print('Special mode already selected')
        pass
    else:
        print('Turning on Special mode')
        DetectAndClick('SpecialOff.jpg')
    time.sleep(0.25)
    DetectAndClick('ok.jpg')
    while SearchButton('random.jpg', timeout = 20) == -1:
        print('Waiting for other players')
        time.sleep(3)
    print('Selecting random song')
    DetectAndClick('random.jpg')
    while SearchButton('ready.jpg',timeout = 20) == -1:
        print('Waiting for song')
        time.sleep(2)
    SelectDifficulty(difficulty)
    DetectAndClick('ready.jpg')

def SelectFree(difficulty):
    time.sleep(0.25)
    DetectAndClick('freeLive.jpg')
    time.sleep(0.25)
    SelectDifficulty(difficulty)
    time.sleep(0.25)
    SelectNoFullComboSong()
    time.sleep(0.5)
    print('Confirming choice')
    DetectAndClick('confirm.jpg')
    time.sleep(0.5)
    DetectAndClick('start.jpg')
    print('lets go')
    time.sleep(0.25)

def SelectVS(difficulty):
    pass

def SelectChallenge(difficulty):
    pass

def SelectMode(mode, difficulty = 'easy'):
    if mode == 'Live':
        SelectLive(difficulty)
    elif mode == 'Free':
        SelectFree(difficulty)
    elif mode == 'VS':
        SelectVS(difficulty)
    elif mode == 'Challenge':
        SelectChallenge(difficulty)
    else:
        pass

try:
    window = win32gui.FindWindowEx(None, None, None, 'NoxPlayer')
    win32gui.MoveWindow(window, 0, 0, 1528, 870, True)
    win32gui.SetForegroundWindow(window)
except Exception:
    print("Nox Player is not turned on")
# initialize the WindowCapture class
wincap = WindowCapture('NoxPlayer')

def Game():
    while(True):
        tmp = DetectAndClick('Button.jpg',region =[666,686,134,1334], timeout = 2000)
        if tmp == -1:
            print('Cannot find clickable button, assuming game ended')
            break
        else:      
            continue
        break

def WaitingConfirm():
    while SearchButton('next.jpg', timeout = 2000) == -1:
        print('Song in progress')
        time.sleep(3)

while True:
    time.sleep(1)
    DetectAndClick('live.jpg')
    SelectFree('easy')

    a = threading.Thread(target = Game)
    a.start()
    b = threading.Thread(target = WaitingConfirm)
    b.start()

    a.join()
    b.join()
    DetectAndClick('next.jpg')
    time.sleep(0.5)
    DetectAndClick('next.jpg')
    time.sleep(0.5)
    DetectAndClick('next.jpg')
    time.sleep(0.5)
